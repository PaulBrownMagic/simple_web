# Simple-Web Framework

Easy, simple websites with Prolog.

Simply declare your routes as predicates and run the server.
Simple-Web aims to help make web development simpler and easier.
It does this by being opinionated. The scope of SWI-Prolog's 
capabilities are narrowed to a core subset, it requires a wise 
directory structure for static files, and it expects that 
you'll use [simple_template](https://github.com/rla/simple-template/tree/master/prolog/st)
for clever work with HTML.

## Setup

Simple-Web depends on simple-templates, this can be installed via

```prolog
?- pack_install(simple_template).
```

## Example Basic Use

Create a directory with `app.pl`

```prolog
:- use_module(sw/simple_web).

sw:route(home, '/', _Request) :-
    reply_html("<h1>Hello, world!</h1>
                <img src='static/images/logo.jpg' alt='logo'/>").

sw:route(templates_test, '/test', _Request) :-
    Data = data{ title: 'Hello'
               , items: [ item{ title: 'Item 1', content: 'Abc 1' }
                        , item{ title: 'Item 1', content: 'Abc 2' }
                        ]
               },
    reply_template(test, Data, options{cache:true}).

sw:route(termarized_test, root(termerized), _Request) :-
    reply_html([title('Termerized')], [h1('Termerized Example'), p('With some text')]).

sw:route(api, '/api', method(get), _Request) :-
    reply_json_dict(data{example: "Hello, world!"}).

:- run([port(5000)]).
```

Inside this root directory, create a folder called `static` to save your
static files, such as your css, javascript and images. Place an image
called `logo.jpg` into static/images/.

Also inside the root directory, create a folder called `templates`, inside
this place `test.html`

```html
<html>
  <head>
    <title>Test</title>
  </head>
  <body>
    <h1>{{= title }}</h1>
    {{ each items, item }}
        <h2>{{= item.title }}</h2>
        <div class="content">{{- item.content }}</div>
    {{ end }}
	<ul>
	  <li><a href="{{ url_for("templates_test")}}"</a></li>
	  <li><a href="{{ url_for("termarized_test")}}"</a></li>
	  <li><a href="{{ url_for("api")}}"</a></li>
	</ul>
  </body>
</html>
```

Finally, run `app.pl` and navigate to http://localhost:5000

```bash
:~$ swipl app.pl
```

## More Examples

A repository of examples, including using static, templates and creating
an API can be found in the [simple_web_examples
repository](https://gitlab.com/PaulBrownMagic/simple_web_examples).

## Predicates

* **run/0**: run the webserver on a random, free, high numbered port
* **run/1**: run the server with options as per
  [http_server/3](http://www.swi-prolog.org/pldoc/man?section=http-running-server#http_server/3)
* **sw:route/3**: Declare a route: sw:route(Name, Path, Request)
  where Request is a list of attr(val)
* **sw:route/4**: Declare a route: sw:route(Name, Path, method(M), Request)
* **reply_html/1**: Reply with a string assumed to be valid html
* **reply_template/2**: Use
  [simple_template](https://github.com/rla/simple-template) to reply:
reply_template(Template, DataDict)
* **read_json_dict/2 &/3**: Read request JSON into dict. See
  [http_read_json_dict](http://www.swi-prolog.org/pldoc/doc_for?object=http_read_json_dict/2).
* **reply_json_dict/1 & 2**: Convert dict to JSON and reply. See
  [read_json_dict](http://www.swi-prolog.org/pldoc/doc_for?object=reply_json_dict/1)
* **reply_file/3**: Reply with a file. See
  [http_reply_file/3](http://www.swi-prolog.org/pldoc/doc_for?object=http_reply_file/3)
* **location/2 &/3**: Define a new URL location, as per
  `http:location(api, "/api", [])`, allows you to declare routes
with `sw:route(all_data, api(all_data), method(get), _Request)`, where
`api(all_data)` is equivalent to `/api/all_data`. See
[http:location/3](http://www.swi-prolog.org/pldoc/man?section=httppath#http:location/3)
* **reply_404/1 &/2**: Return a 404, eg: reply_404(Request). See
  [http_404/2](http://www.swi-prolog.org/pldoc/doc_for?object=http_404/2)
* **reply_redirect/3**: Redirect to another page, eg:
  reply_redirect(How, Where, Request). How is one of `moved`,
`moved_temporary` or `see_other`. Where can be an atom ('/some/page'),
or an aliased path (root(some/page)). See [http_redirect/3](http://www.swi-prolog.org/pldoc/doc_for?object=http_redirect/3)

## Config Options

You can optionally create a `config.pl` file in the root
directory of your application to change the default values:

For simple_web:

* `config(debug, true).` Turn on [http/http_error](http://www.swi-prolog.org/pldoc/man?section=http-debug). *Default false*
* `config(static_dir, "/static").` Name of the directory in the web
  application root directory to serve static files from. *Default
"/static"*
* `config(templates_dir, "/templates").` Name of the directory in the
  root directory in which templates are stored. *Default
"/templates"*

For simple_template, these are set to the same default's as
simple_template and are documented by
[simple_template](https://github.com/rla/simple-template/tree/master/prolog/st):

* `config(st_encoding, utf8).`
* `config(st_extension, html).`
* `config(st_cache, false).`
* `config(st_strip, false).`
* `config(st_frontend, simple).`
* `config(st_undefined, error).`
