/** <module> Simple Web

Easy, simple websites with Prolog.

## Example basic use

Create a directory with `app.pl`

```prolog
:- use_module(sw/simple_web).

sw:route(home, '/', _Request) :-
    reply_html("<h1>Hello, world!</h1>
                <img src='static/images/logo.jpg' alt='logo'/>").

sw:route(template, '/test', _Request) :-
    Data = data{ title: 'Hello'
               , items: [ item{ title: 'Item 1', content: 'Abc 1' }
                        , item{ title: 'Item 1', content: 'Abc 2' }
                        ]
               },
    reply_template(test, Data).

sw:route(termarized, root(termerized), _Request) :-
    reply_html([title('Termerized')], [h1('Termerized Example'), p('With some text')]).

sw:route(api, '/api', method(get), _Request) :-
    reply_json_dict(data{example: "Hello, world!"}).

:- run([port(5000)]).
```

Inside this root directory, create a folder called `static` to save your
static files, such as your css, javascript and images. Place an image
called `logo.jpg` into static/images/.

Also inside the root directory, create a folder called `templates`, inside
this place `test.html`

```html
<html>
  <head>
    <title>Test</title>
  </head>
  <body>
    <h1>{{= title }}</h1>
    {{ each items, item }}
        <h2>{{= item.title }}</h2>
        <div class="content">{{- item.content }}</div>
    {{ end }}
    <ul>
        <li><a href="{{= url_for('home') }}">Home</a></li>
        <li><a href="{{= url_for('termarized') }}">Termarized Example</a></li>
        <li><a href="{{= url_for('api') }}">API Example</a></li>
    </ul>
  </body>
</html>
```

Finally, run `app.pl` and navigate to http://localhost:5000

```bash
:~$ swipl app.pl
```

## More Examples

A repository of examples, including using static, templates and creating
an API can be found in the [simple_web_examples
repository](https://gitlab.com/PaulBrownMagic/simple_web_examples).



@author Paul Brown
@license MIT
@version 0.1.4
*/

:- module(simple_web,
    [ run/0
    , run/1
    , read_json_dict/2
    , read_json_dict/3
    , reply_json_dict/1
    , reply_json_dict/2
    , reply_404/1
    , reply_404/2
    , reply_redirect/3
    , url_for/2
    ]
).

:-  ( user:file_search_path(sw_app, _) ->
      true
    ;
      working_directory(Dir, Dir),
      asserta(user:file_search_path(sw_app, Dir))
    ).

:- use_module(sw_config_handling).

:- reexport(sw).
:- reexport(sw_static_handling).
:- use_module(sw_route_handling).

:- reexport(sw_template_handling).
:- use_module(library(http/http_json),
    [ http_read_json_dict/2 as read_json_dict
    , http_read_json_dict/3 as read_json_dict
    , reply_json_dict/1
    , reply_json_dict/2
    ]
).
:- use_module(library(http/http_dispatch),
    [ http_404/2 as reply_404
    , http_redirect/3 as reply_redirect
    ]
).

%! reply_404(Options, Request) is det.
%  @arg Options as per http_404/2.
%! reply_404(Request) is det.
%  Respond with a 404, no options, as per
%  http_404/2
reply_404(Request) :-
    http_404([], Request).

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- ( get_config(debug, true) ->
     use_module(library(http/http_error))
   ; true).

%! run is det.
%  run the webserver on a random free port
run :-
    sw_run([port(_)]), !.

%! run(+Options) is det.
%  run the webserver with Options
%
%  @arg Options as per http_server/3
run(Options) :-
    is_list(Options),
    sw_run(Options), !.
run(Options) :-
    \+ is_list(Options),
    sw_run([Options]), !.

%! sw_run(+Options) is det.
%  init routes and run webserver
sw_run(Options) :-
    init_routes,
    http_server(http_dispatch, Options), !.
