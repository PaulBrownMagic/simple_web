/** <module> Template Handling

Run template handling from template directory.
Uses simple-template: https://github.com/rla/simple-template

*/

:- module(sw_template_handling,
    [ reply_html/1
    , reply_html/2
    , reply_html/3
    , reply_template/2
    , reply_template/3
    ]
).

:- use_module(library(st/st_expr)).
:- use_module(library(st/st_render)).

:- use_module(sw_config_handling).
:- use_module(library(http/html_write)).

:- use_module(sw_route_handling,
    [ url_for/2
    , url_for/3
    ]
).

:- get_config(templates_dir, Dir),
   asserta(user:file_search_path(sweb_templates, sw_app(Dir))).

:- st_set_function(url_for, 1, url_for).
:- st_set_function(from_templates, 1, from_templates).

from_templates(File, Path) :-
    absolute_file_name(sweb_templates(File), Path, []).

%! reply_html(HTML:str) is det
%
%  Reply with raw string as html
%
%  @arg HTML a string assumed to be HTML
%! reply_html(:Head, :Body) is det
%
%  wrapper to reply_html_page/2
%  replies with termerized html
%! reply_html(+Style, :Head, :Body) is det
%
%  wrapper to reply_html_page/3
%  replies with termerized html
reply_html(HTML) :-
    string(HTML),
    format("Content-type: text/html~n~n~w~n", HTML).
reply_html(Head, Body) :-
    reply_html_page(Head, Body).
reply_html(Style, Head, Body) :-
    reply_html_page(Style, Head, Body).

%! reply_template(+Template:atom, +Data:dict) is det.
%
%  Reply with simple-template
%
%  @arg Template The name of the template, ".html" not required
%  @arg Data Dict of keys and values used in the template
reply_template(Template, Data) :-
    st_options(Options),
    render_reply(Template, Data, Options).

%! reply_template(+Template:atom, +Data:dict, +Options:dict) is det.
%
%  Reply with simple-template, providing simple-template options:
%
%  * encoding - default config utf8.
%  * extension - file name extension, default config html.
%  * cache - whether to use cache, default config false.
%  * strip - whether to try to strip whitespace from output, default config false.
%  * frontend - which syntax to use, currently simple (default config) or semblance.
%  * undefined - throw error on undefined variables - error (default config) or false (evaluates to false).
%
%  @arg Template The name of the template, ".html" not required
%  @arg Data Dict of keys and values used in the template
%  @arg Options Dict of options as described for simple-template
reply_template(Template, Data, Options) :-
    st_options(Config),
    Opts = Config.put(Options),
    render_reply(Template, Data, Opts).

%! render_reply(+Template:atom, +Data:dict, +Options:dict) is det.
%
%  Makes the template and sends the reply with text/html heading.
%
%  @arg Template The name of the template, ".html" not required
%  @arg Data Dict of keys and values used in the template
%  @arg Options Dict of options as described for simple-template
render_reply(Template, Data, Options) :-
    current_output(Out),
    format("Content-type: text/html~n~n"),
    st_render_file(sweb_templates(Template), Data, Out, Options).

%! st_options(-Options:dict) is semidet.
%
%  @arg Options Simple Template options from config
st_options(options{ encoding: Encoding,
                    extension: Extension,
                    cache: Cache,
                    strip: Strip,
                    frontend: Frontend,
                    undefined: Undefined}) :-
    get_config(st_encoding, Encoding),
    get_config(st_extension, Extension),
    get_config(st_cache, Cache),
    get_config(st_strip, Strip),
    get_config(st_frontend, Frontend),
    get_config(st_undefined, Undefined).
