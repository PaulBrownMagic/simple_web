:- module(sw,
    [ location/2
    , location/3
    , route/3
    , route/4
    ]
).

:- dynamic route/3.
:- dynamic route/4.
:- dynamic handler_url/2.
:- multifile route/3.
:- multifile route/4.
:- multifile handler_url/2.

%! location(Name, Url, Options) is det.
%  assert http:location/3
%! location(Name, Url) is det.
%  assert http:location/3 with default options
location(Name, Url) :-
    asserta((http:location(Name, Url, []))).
location(Name, Url, Options) :-
    asserta((http:location(Name, Url, Options))).

