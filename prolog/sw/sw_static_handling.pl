/** <module> Static Handling

Setup static serving from static directory

*/

:- module(sw_static_handling,
    [ reply_file/2
    , reply_file/3
    ]
).

:- use_module(sw_config_handling).

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_files)).

http:location(static, "/static", []).

:- get_config(static_dir, Dir),
   http_handler(static(.),
                http_reply_from_files(sw_app(Dir), []),
                [prefix]).

%! reply_file(+File, +Request) is semidet.
%
%  Respond with a file using default options
%
%  @arg File The path to the file to respond with,
%  relative or absolute.
%  @arg Request The Request provided to the route
reply_file(File, Request) :-
    reply_file(File, [], Request).

%! reply_file(+File, +Request) is semidet.
%
%  Respond with a file with options as per [http_reply_file/3]
%  from library(http/http_dispatch)
%
%  @arg File The path to the file to respond with,
%  relative or absolute.
%  @arg Options As per http_reply_file/3
%  @arg Request The Request provided to the route

reply_file(File, Opts, Request) :-
    http_reply_file(sw_app(File), Opts, Request), !.
reply_file(File, Opts, Request) :-
    http_reply_file(File, Opts, Request).
