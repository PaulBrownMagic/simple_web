/** <module> Config Handling

Set framework attributes, looks for a config.pl
file in project and assert facts. Otherwise, set
reasonable defaults.

*/

:- module(sw_config_handling,
    [ get_config/2
    ]
).

:- ( absolute_file_name(sw_app('config.pl'), P),
     exists_file(P) ->
     consult(P) ;
     assertz(config(none, none))
   ).

%! get_config(+Key:atom, -Val:atom) is nondet
%
%  User config, else default provided.
%
%  @arg Key Name of config attribute
%  @arg Val Value of config attribute
get_config(Key, Val) :-
    config(Key, Val), !.
get_config(Key, Val) :-
    default(Key, Val).

%! default(+Key:atom, +Val:atom) is nondet.
%
%  @arg Key Name of config attribute
%  @arg Val Value of config attribute
default(static_dir, static).
default(templates_dir, templates).
default(st_encoding, utf8).
default(st_extension, html).
default(st_cache, false).
default(st_strip, false).
default(st_frontend, simple).
default(st_undefined, error).
