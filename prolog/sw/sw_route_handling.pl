/** <module> Route Handling

Make a route predicate in the project
handle the route and write out the
response to current out

*/

:- module(sw_route_handling,
    [ init_routes/0
    , url_for/2
    , url_for/3
    ]
).

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_path)).

%! init_routes is det
%
%  find all simple_web:routes and setup
%  http_handlers for them.
init_routes :-
    findall(N-R, clause(sw:route(N, R, _Request), _), RoutesS),
    findall(N-R-M, clause(sw:route(N, R, M, _Req), _), RoutesM),
    append(RoutesS, RoutesM, Routes),
    handle_routes(Routes).

%! handle_routes(+Routes) is det
%
%  recursively handle a route
%
%  @arg Routes A list of routes
handle_routes([]).
handle_routes([R| Routes]) :-
    handle_route(R),
    handle_routes(Routes).

%! handle_route(+Route:atom) is det
%  create http_handler for Route
%
%  @arg Route an atom /route/to/page
handle_route(N-R-M) :-
    log_route(N, R),
    http_handler( R,
                  method_route(N, R, M),
                  []).
handle_route(N-R) :-
    \+ R = _-_,
    \+ N = _-_,
    ground(R),
    log_route(N, R),
    http_handler( R,
                  sw:route(N, R),
                  []).
handle_route(N-R) :-
    \+ R = _-_,
    \+ N = _-_,
    \+ ground(R),
    log_route(N, R),
    R =.. [P|_],
    Route =.. [P, '.'],
    http_handler( Route,
                  var_route(N, P),
                  [prefix]).

%! log_route(Name, Route) is det.
%  log the handler for the route to enable url_for
log_route(Name, Route) :-
    asserta(sw:handler_url(Name, Route)).
% unused, route is route without method
log_route(Name, Route, Method) :-
    asserta(sw:handler_url(Name, Route, Method)).

%! url_for(+Handler, ?URL) is semidet.
%  Find the URL for a given handler name,
%  also can be used with static files: url_for("static('js/main.js')")
%! url_for(+Handler, ?URL, +Options) is semidet.
%  Find the URL for a given handler name
%
%  @arg Options Is a list of options. Only supported
%  option is `absolute`, which returns an absolute URL
url_for("static", "/static").
url_for(StaticPath, URL) :-
    term_string(static(Path), StaticPath),
    string_concat("/static/", Path, URL).
url_for(Handler, URL) :-
    \+ string(Handler),
    sw:handler_url(Handler, Route),
    http_absolute_location(Route, URL, [relative_to(root)]).
url_for(SHandler, URL) :-
    string(SHandler), term_string(Handler, SHandler),
    sw:handler_url(Handler, Route),
    http_absolute_location(Route, URL, [relative_to(root)]).
url_for(Handler, URL, []) :-
    url_for(Handler, URL).
url_for(Handler, URL, [absolute]) :-
    ground(Handler),
    sw:handler_url(Handler, Route),
    http_absolute_uri(Route, URL).
% url_for(static(Path), URL)


%! method_route(+Route, +Method, +Request) is semidet.
%  route with a method
%
%  @arg Route an atom route/to/page
%  @arg Method term: method(get), method(post) etc.
%  @arg Request
method_route(N, R, M, Request) :-
    member(M, Request),
    sw:route(N, R, M, Request), !.
method_route(_, _, _, Request) :-
    http_404([], Request).

%! var_route(+Predicate, +Request) is semidet.
%  handle variables in a route, extract data from
%  path info, split by '/'.
var_route(N, P, Request) :-
    member(path_info(A), Request),
    atomic_list_concat(Args, '/', A),
    R =.. [P|Args],
    sw:route(N, R, Request).
var_route(_, _, Request) :-
    http_404([], Request).
