name(simple_web).
version('0.3.1').
title('Microframework for building websites').
author('Paul Brown', 'paul@paulbrownmagic.com').
home('https://gitlab.com/PaulBrownMagic/simple_web').
download('https://gitlab.com/PaulBrownMagic/simple_web/*').
requires('simple_template').
